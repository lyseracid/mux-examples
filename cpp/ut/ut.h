#pragma once

#include <map>
#include <string>
#include <functional>
#include <iostream>

class Tests {
public:
    void test(const char* name, std::function<void()> test) {
        m_tests.emplace(name, std::move(test));
    }

    template<typename Message>
    void fail(Message error) {
        std::cout << "Fail: " << error << std::endl;
        m_hasErrors = true;
    }

    void run() {
        size_t failedCount = 0;
        for (const auto& [name, test]: m_tests) {
            std::cout << "[ " << name << " ] STARTED"<< std::endl;
            test();
            std::cout << "[ " << name << " ] " << (m_hasErrors ? "FAILED" : "PASSED") << std::endl;
            if (m_hasErrors) ++failedCount;
            m_hasErrors = false;
        }
        if (failedCount == 0)
            std::cout << "All " << m_tests.size() << " tests passed!" << std::endl;
        else
            std::cout << failedCount << " of " << m_tests.size() << " tests has failed!" << std::endl;
    }

private:
    std::map<const char*, std::function<void()>> m_tests;
    bool m_hasErrors = false;
};

extern Tests g_tests;

#define TEST(NAME, BODY) int NAME = [] { g_tests.test(#NAME, BODY); return 1; }();
#define ASSERT(CONDITION) if (!(CONDITION)) { g_tests.fail(#CONDITION); }
