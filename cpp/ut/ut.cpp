#include "ut.h"

Tests g_tests;

int main() {
    g_tests.run();
}

TEST(SuccessfulTest, [] {
    ASSERT(2 + 2 == 4);
    ASSERT(2 + 3 == 5);
})

TEST(FaultyTest, [] {
    ASSERT(2 + 2 == 5);
    ASSERT(2 + 3 == 6);
})
