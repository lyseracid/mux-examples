#/usr/bin/bash

set -e
set -x

clear
g++ -std=c++17 -Wall -Werror *.cpp -o stream

printf abbcacfg | ./stream 1
printf bcfbfcbccfccbb | ./stream 2

rm stream
