#include "first_n_repeating_stream.h"

#include <limits>


#include <iostream>

FirstNRepeatingStream::FirstNRepeatingStream(int N): m_maxCount(N) {
    m_order.reserve(std::numeric_limits<unsigned char>::max());
    m_counts.resize(std::numeric_limits<unsigned char>::max(), 0);
}

std::vector<int>::iterator FirstNRepeatingStream::count(char c)
{
    //std::cerr << "count " << c << ": " << *std::next(m_counts.begin(), static_cast<unsigned char>(c)) << std::endl;
    return std::next(m_counts.begin(), static_cast<unsigned char>(c));
}

std::optional<char> FirstNRepeatingStream::next(char c) {
    const auto lastCount = count(c);

    // This protects against overflow, maximum is negative and will newer be printed.
    if (*lastCount >= 0 && *lastCount <= m_maxCount)
        (*lastCount)++;

    // Save the order if it's the first apperance, m_firstIndex will be autocorected of it's in the end.
    if (*lastCount == 1)
        m_order.push_back(c);

    for (; m_firstIndex < m_order.size(); ++m_firstIndex)
    {
        const auto firstChar = m_order[m_firstIndex];
        //std::cerr << "index " << m_firstIndex << " char " << firstChar << std::endl;
        if (const auto firstCount = *count(firstChar); firstCount > 0 && firstCount <= m_maxCount)
            return firstChar;
    }

    return std::nullopt;
}
