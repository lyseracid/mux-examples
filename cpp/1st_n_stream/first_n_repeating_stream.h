#pragma once

#include <array>
#include <vector>
#include <optional>

/**
 * Processes the stream of characters, returns the first given character appering no more then
 * N times or std::nullptr.
 */
class FirstNRepeatingStream {
public:
    FirstNRepeatingStream(int N);
    std::optional<char> next(char c);

private:
    std::vector<int>::iterator count(char c);

private:
    int m_maxCount = 0;
    std::vector<char> m_order;
    size_t m_firstIndex = 0;
    std::vector<int> m_counts{0};
};
