#include <iostream>

#include "first_n_repeating_stream.h"

int main(int argc, char** argv) {
    int maxCount = 0;
    try {
        if (argc != 2)
            throw std::invalid_argument("argc");
        if (maxCount = std::stoi(argv[1]); maxCount <= 0)
            throw std::invalid_argument("negative");
    } catch (const std::invalid_argument& e) {
        std::cerr << "ERROR: Invalid argument: " << e.what() << std::endl;
        std::cerr << "Usage: " << argv[0] << " [NUMBER]" << std::endl;
        return 1;
    }

    FirstNRepeatingStream stream(maxCount);
    for (auto input = std::cin.get(); input != -1; input = std::cin.get()) {
        if (const auto out = stream.next((char) input))
            std::cout << (char) *out << std::endl;
        else
            std::cout << "nullopt" << std::endl;
    }
}
