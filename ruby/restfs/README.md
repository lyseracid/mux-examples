# File System REST API

## Dependencies

Ubuntu
```
snap install ruby
bundle install
```

## Run

```
bundle exec ./server.rb [directory_path]
```
