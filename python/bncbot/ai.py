#!/usr/bin/python

import os

import numpy
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import LSTM, GRU, Bidirectional, Dense, Dropout


class Model:
    FIELDS = ['Close']

    def __init__(self, cache_path, seq_len=100, future_steps=5, train_limit=1000, predict_limit=1000,
        compile=dict(optimizer='adam', loss='mean_squared_error'), fit=dict(epochs=20, batch_size=32),
    ):
        self.cache_path = cache_path
        self.seq_len, self.future_steps = seq_len, future_steps
        self.compile, self.fit = compile, fit
        self.train_limit, self.predict_limit = train_limit, predict_limit
        self.scaler = MinMaxScaler(feature_range=(0, 1))
        try:
            self.model = load_model(self.cache_path)
            print(f'*** AI MODEL LOADED {self.cache_path}')
        except Exception as e:
            print(f'*** NO MODEL AT {self.cache_path} -- {e}')

    def save(self):
        self.model.save(self.cache_path)
        print(f'*** AI MODEL SAVED {self.cache_path}')

    def train(self, data):
        pass

    def predict(self, last_data):
        return []

    def _train_and_save(self, x, y):
        self.model.compile(**self.compile)
        self.model.fit(numpy.array(x), numpy.array(y), **self.fit)
        self.save()


class SimpleModel(Model):
    FIELDS = ['Close']

    def train(self, data):
        scaled = self.scaler.fit_transform(data)
        x, y = [], []
        for i in range(len(scaled) - self.seq_len):
            x.append(scaled[i:i + self.seq_len])
            y.append(scaled[i + self.seq_len, 0])

        self.model = Sequential([
            LSTM(50, return_sequences=True, input_shape=(self.seq_len, 1)), Dropout(0.2),
            LSTM(50, return_sequences=False), Dropout(0.2),
            Dense(25), Dense(1)
        ])
        self._train_and_save(x, y)

    def predict(self, last_data):
        scaled = self.scaler.fit_transform(last_data)
        predicted = self.scaler.inverse_transform(self.model.predict(scaled))
        return tuple(float(p[0]) for p in predicted[:self.future_steps])


class BidirectionalModel(Model):
    FIELDS = ['Open', 'High', 'Low', 'Close', 'Volume']

    def train(self, data):
        scaled = self.scaler.fit_transform(data)
        x, y = [], []
        for i in range(len(scaled) - self.seq_len - self.future_steps):
            x.append(scaled[i:i + self.seq_len])
            y.append(scaled[i + self.seq_len:i + self.seq_len + self.future_steps, 3])

        self.model = Sequential([
            Bidirectional(LSTM(64, return_sequences=True), input_shape=(self.seq_len, 5)),
            Dropout(0.2), GRU(64, return_sequences=False), Dropout(0.2),
            Dense(32, activation="relu"), Dense(self.future_steps)
        ])
        self._train_and_save(x, y)

    def predict(self, last_data):
        scaled = numpy.array([self.scaler.fit_transform(last_data[-self.seq_len:])])
        predicted_price = self.model.predict(scaled)[0]
        padded_array = numpy.zeros((self.future_steps, 5))
        padded_array[:, 3] = predicted_price.reshape(self.future_steps, 1)[:, 0]
        return tuple(float(v) for v in self.scaler.inverse_transform(padded_array)[:, 3])


def make_model(name='simple', cache_path='~/.binance/any', **args):
    path = f'{os.path.expanduser(cache_path)}_{name}.keras'
    if name == 'simple': return SimpleModel(path, **args)
    if name == 'bidir': return BidirectionalModel(path, **args)
    raise KeyError(f'Unsupported model: {name}')
