#!/usr/bin/python

import argparse
import yaml
import os
import time

from ai import make_model
from trader import Trader


class Bot:
    def __init__(self, name, api_keys, cache='~/.binance/', model={}, expect_profit=0.001, **trade):
        self.name = name
        self.expect_profit = float(expect_profit)
        self.trader = Trader(api_keys, **trade)
        self.model = make_model(cache_path=os.path.join(cache, self.trader.symbol), **model)

    @staticmethod
    def load(keys_path, config_path, bot_name):
        with open(os.path.expanduser(keys_path)) as f: keys = yaml.safe_load(f)
        with open(os.path.expanduser(config_path)) as f: config = yaml.safe_load(f)
        bot_config = config.pop('bots')[bot_name]
        return Bot(bot_name, keys, **bot_config, **config)

    def train(self):
        d = self.trader.price_data(self.model.train_limit, self.model.FIELDS)
        self.model.train(d)
        self.predict()
        print(f'[{self.name}] Train data:\n{d}')

    def predict(self):
        r = self.model.predict(self.trader.price_data(self.model.predict_limit, self.model.FIELDS))
        print(f'[{self.name}] Predicted: {r}')
        return r

    def diff(self):
        predicted_prices = self.predict()
        current_price = self.trader.price()
        diff = 0
        for price in predicted_prices:
            new_diff = price - current_price
            if new_diff * diff < 0: break #< Different direction.
            diff = max(diff, new_diff) if new_diff > 0 else min(diff, new_diff)
        diff_p = abs(round(diff * 100 / current_price, 2))
        print(f'[{self.name}] Current price: {current_price}, max predicted diff: {diff} - {diff_p}%')
        return current_price, diff

    def trade(self):
        if self.trader.is_last_trade_done():
            price, diff = self.diff()
            if abs(diff) >= price * self.expect_profit:
                self.trader.place_trade_diff(price, diff)

    def cycle(self):
        print(f'[{self.name}] Running trade cycle...')
        while True:
            self.trade()
            time.sleep(1)


if __name__ == '__main__':
    runs = ', '.join([m for m in dir(Bot) if not m.startswith('_')])
    parser = argparse.ArgumentParser(description='Binance Trading Bot')
    parser.add_argument('-k', '--keys-path', help='Binance API keys', default='~/.binance/test.yaml')
    parser.add_argument('-c', '--config-path', help='Bots Config', default='./config.yaml')
    parser.add_argument('-b', '--bot-name', help='Bot profile name', default='btc')
    parser.add_argument('-a', '--action', help=f'One of: {runs}', default='diff')

    args = parser.parse_args()
    print(f'>>> Run {args.action} on {args.keys_path} with {args.config_path}')
    bot = Bot.load(args.keys_path, args.config_path, args.bot_name)
    func = getattr(bot, args.action)
    try: func()
    except KeyboardInterrupt: print('>>> Exited')
