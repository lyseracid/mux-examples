#!/usr/bin/python

import binance.client
import pandas


class Trader:
    PRICE_COLUMNS = ['Time', 'Open', 'High', 'Low', 'Close', 'Volume', '_', '_', '_', '_', '_', '_']

    def __init__(self, api_keys: dict, symbol='BTCUSDT', quantity=0.002,
        loss_profit=(0.25, 0.75), data_interval='15m'
    ):
        self.client = binance.client.Client(**api_keys)
        self.symbol, self.quantity = symbol, float(quantity)
        self.stop_loss, self.take_profit = loss_profit
        self.data_interval = data_interval

    def price(self):
        return float(self.client.futures_symbol_ticker(symbol=self.symbol)['price'])

    def price_data(self, limit=1000, fields=['Close']):
        klines = self.client.futures_klines(symbol=self.symbol, interval=self.data_interval, limit=limit)
        return pandas.DataFrame(klines, columns=self.PRICE_COLUMNS)[fields].astype(float)

    def place_order(self, action='BUY', type='MARKET', price=None, **args):
        r = self.client.futures_create_order(
            symbol=self.symbol, side=action, type=type, quantity=self.quantity, **args)
        print(f'${self.symbol}$ {r['orderId']}: {action} {self.quantity} on {type} with {args}')
        return r

    def cancel_order(self, id):
        self.client.futures_cancel_order(symbol=self.symbol, orderId=id)
        print(f'${self.symbol}$ {id}: Canceled outstanding order')

    def place_trade(self, action, stop_loss: float, take_profit: float):
        rev_action = 'SELL' if action == 'BUY' else 'BUY'
        self.place_order(action)
        orders = []
        try:
            orders += [self.place_order(rev_action, type="TAKE_PROFIT_MARKET", stopPrice=take_profit)]
            orders += [self.place_order(rev_action, type="STOP_MARKET", stopPrice=stop_loss)]
        except binance.exceptions.BinanceAPIException as e:
            print(f'${self.symbol}$ Error: {e}')
            self.place_order(rev_action)
            for d in orders: self.cancel_order(d['orderId'])

    def place_trade_diff(self, price: float, p_diff: float):
        action = 'BUY' if p_diff > 0 else 'SELL'
        stop_loss = round(price - p_diff * self.stop_loss, 2)
        take_profit = round(price + p_diff * self.take_profit, 2)
        print(f'${self.symbol}$ {action} for {price}, stop loss {stop_loss}, take profit {take_profit}')
        self.place_trade(action, stop_loss=stop_loss, take_profit=take_profit)

    def is_last_trade_done(self):
        orders = self.client.futures_get_open_orders(symbol=self.symbol)
        if len(orders) > 2:
            raise ValueError(f'Unsupported state, too mary orders: {len(orders)}')
        if len(orders) == 2:
            return False
        if len(orders) == 1:
            self.cancel_order(orders[0]['orderId'])
        return True
