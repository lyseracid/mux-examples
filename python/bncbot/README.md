# Bibance Trading Bot example

## Setup venv and see help

```
python -m venv .venv
. .venv/bin/activate
pip install -r requirements.txt

python bot.py -h
python test.py -h
```

## Example keys

Default path `~/.binance/test.yaml`
```
api_key: BINANCE_API_KEY
api_secret: BINANCE_API_SECRET
testnet: true #< Remove to run on actual account.
```

## TODO

* Several bots in 1 config
* Auto-close deals by time limit
* Auto-clode deals if expectations change
* Train on more data (several requests)
