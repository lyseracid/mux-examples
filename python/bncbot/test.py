#!/usr/bin/python

import argparse
import yaml
import os
import time
import random

from bot import Bot
import pandas


class TraderHucks:
    REC_SHIFT = 4
    SHIFT_ITERATIONS = 10

    def __init__(self, bot: Bot, time_shift: str):
        self.bot = bot
        self.bot.trader.price = lambda *a, **kw: self.price(*a, **kw)
        self.bot.trader.price_data = lambda *a, **kw: self.price_data(*a, **kw)
        self.bot.trader.place_trade = lambda *a, **kw: self.place_trade(*a, **kw)
        self.time_shift = self._duration(time_shift)
        self._price_cache = None
        self._price_pos = 0
        self._price_current = None
        self._last_price = None
        self.stats = {'profit': 0, 'loss': 0, 'none': 0, 'change': 0}

    def price(self):
        self._last_price = self._price_current.at[len(self._price_current) - 1, 'Close']
        return self._last_price

    def price_data(self, limit, fields):
        if self._price_cache is None:
            interval = self.bot.trader.data_interval
            max_limit = self.bot.model.train_limit + self.REC_SHIFT * (self.SHIFT_ITERATIONS + 1)
            klines = self.bot.trader.client.futures_klines(
                symbol=self.bot.trader.symbol, interval=interval, limit=max_limit,
                startTime=self._start_time(interval, max_limit))
            self._price_cache = pandas.DataFrame(klines, columns=self.bot.trader.PRICE_COLUMNS) \
                [fields].astype(float)
            self._price_pos = self.REC_SHIFT * self.SHIFT_ITERATIONS

        self._price_current = self._price_cache[:-self._price_pos]
        self._price_pos += self.REC_SHIFT
        return self._price_current[-limit:]

    def place_trade(self, action, stop_loss: float, take_profit: float):
        def what():
            actual = self._price_cache[-self._price_pos:]['Close']
            print('Actual', tuple(actual[:10]))
            for p in actual:
                if action == 'BUY':
                    if p <= stop_loss: return 'loss', stop_loss - self._last_price
                    elif p >= take_profit: return 'profit', take_profit - self._last_price
                else:
                    if p >= stop_loss: return 'loss', self._last_price - stop_loss
                    elif p <= take_profit: return 'profit', self._last_price + take_profit
            return 'none', 0
        result, change = what()
        print(f'!!! {result} {change}')
        self.stats[result] += 1
        self.stats['change'] += float(change * self.bot.trader.quantity)


    @staticmethod
    def _duration(duration: str, limit: int = 1) -> int:
        time_units = {'m': 60, 'h': 3600, 'd': 86400, 'w': 604800}
        unit = duration[-1]
        value = int(duration[:-1])
        return value * time_units[unit] * limit

    def _start_time(self, duration: str, limit: int = 1):
        return int(time.time() - self.time_shift - self._duration(duration, limit)) * 1000

    @staticmethod
    def test(bot: Bot, time_shift: str, skip_train=False):
        hucks = TraderHucks(bot, time_shift)
        if not skip_train:
            bot.train()
        for _ in range(hucks.SHIFT_ITERATIONS):
            bot.trade()
        print('Stats', hucks.stats)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Binance Trading Bot Test on historical data')
    parser.add_argument('-k', '--keys-path', help='Binance API keys', default='~/.binance/test.yaml')
    parser.add_argument('-c', '--config-path', help='Bots Config', default='./config.yaml')
    parser.add_argument('-b', '--bot-name', help='Bot profile name', default='btc')
    parser.add_argument('-t', '--time-shift', help='Time shift for historical data', default='24h')
    parser.add_argument('-s', '--skip-train', help='Skip model traning', action='store_true', default=False)

    args = parser.parse_args()
    bot = Bot.load(args.keys_path, args.config_path, args.bot_name)
    TraderHucks.test(bot, args.time_shift, args.skip_train)

