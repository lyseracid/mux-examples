#!/usr/bin/python
'''SSL Connecting proxy for old style HTTP/RTSP clients w/o Bearer token support

Run proxy: python ssl_proxy {server}:{port} {local_port}
Run HTTP request: curl http://localhost:{local_port}/{request}
Run RTSP client: vlc rtsp://localhost:{local_port}/{request}
'''

import socket
import ssl
import threading

RECV_TIMEOUT = 0.1
RECV_BUFFER_SIZE = 1024

AUTH_BEGIN='Authorization: Digest username="'
AUTH_USER_END='"'
AUTH_END='\r\n'
AUTH_REPLACEMENT='Authorization: Bearer '


class SslProxy:
    def __init__(self, remote_addr, local_addr, use_bearer=True, ignore_ssl_cert=True):
        self._server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._remote_addr = self._parse_addr(remote_addr)
        self._local_addr = self._parse_addr(local_addr)
        self._client_threads = []
        self._use_bearer = use_bearer
        self._ssl = ssl.create_default_context()
        if ignore_ssl_cert:
            self._ssl.check_hostname = False
            self._ssl.verify_mode = ssl.CERT_NONE

    def __enter__(self):
        self._server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._local_addr = self._server.bind(self._local_addr) or self._local_addr
        self._server.listen()
        return self

    def __exit__(self, *args):
        self._server.close()
        for t in self._client_threads:
            t.join()

    def serve(self):
        print(f'Starting proxy on {self._local_addr} -> {self._remote_addr}')
        while True:
            client, client_addr = self._server.accept()
            thread = threading.Thread(target=lambda: self._serve_client(client, client_addr))
            thread.start()
            self._client_threads.append(thread) #< TODO Cleanup on thread exit.

    def _parse_addr(self, addr):
        p = addr.split(':')
        return ('127.0.0.1', int(addr)) if len(p) == 1 else (p[0], int(p[1]))

    def _serve_client(self, client, client_addr):
        with socket.create_connection(self._remote_addr) as connection:
            with self._ssl.wrap_socket(connection, server_hostname=self._remote_addr[0]) as target:
                client.settimeout(RECV_TIMEOUT)
                target.settimeout(RECV_TIMEOUT)
                try:
                    print(f'Proxy {client_addr} <-> {self._remote_addr} started')
                    while True:
                        if data := self._read_socket(client):
                            target.send(self._replace_auth_header(data))
                        if data := self._read_socket(target):
                            client.send(data)

                except EOFError:
                    pass

                finally:
                    print(f'Proxy {client_addr} <-> {self._remote_addr} stopped')

    def _read_socket(self, socket):
        try:
            data = socket.recv(RECV_BUFFER_SIZE)
            if not data:
                raise EOFError('disconnected')
            return data
        except OSError as e:
            if 'timeout' not in type(e).__name__: raise
            return None

    def _replace_auth_header(self, data):
        if not self._use_bearer:
            return data

        data_str = data.decode('utf-8')
        print('----- IN:\r\n' + data_str)
        data_out = self._replace_auth_header_str(data_str)
        print('----- OUT:\r\n' + data_out)
        return data_out.encode('utf-8')

    def _replace_auth_header_str(self, data):
        auth_begin = data.find(AUTH_BEGIN)
        if auth_begin == -1:
            return data

        auth_end = data.find(AUTH_END, auth_begin)
        if auth_end == -1:
            return data

        auth_user_begin = auth_begin + len(AUTH_BEGIN)
        auth_user_end = data.find(AUTH_USER_END, auth_user_begin)
        if auth_user_end == -1:
            return data

        auth = AUTH_REPLACEMENT + data[auth_user_begin:auth_user_end]
        return data[:auth_begin] + auth + data[auth_end:]


def main():
    import sys
    target_addr = sys.argv[1] if len(sys.argv) > 1 else '127.0.0.1:7011'
    source_addr = sys.argv[2] if len(sys.argv) > 2 else '127.0.0.1:7777'
    with SslProxy(target_addr, source_addr) as proxy:
        proxy.serve()


if __name__ == '__main__':
    main()
