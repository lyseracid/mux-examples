# Zombie Attack - kivy based zombie shooter game!

Optimized for a touch screen, the main target platform Android 4+.
However any kivy platform is supported automatically.

Kivy runs on Linux, Windows, OS X, Android and iOS. -> http://kivy.org
