# SHoooT!

The puzzle game where you have to think before shooot :)

![](android/ic_launcher-web.png)

In each level, you have to arrange tiles so the cannonball destroys target when you shoot. All
objects are movable, except ones mounted on the brick wall. Levels are randomly generated so you
should not get bored ever.

![](android/feature.png)

## Build and Run

- Install Android Studio or Intellij IDEA for builtin kotlin compiler and build system.
- Desktop configuration should use main class `com.mux.shooot.desktop.DesktopLauncher` and working
  directory `$SRC_ROOT/android/assets`, then build and run.
- Android configuration should be ready as soon as you open project in IDEA, just build and run.
- TODO: HTML5 configuration.

## Development Plan

- AdMob integration and re-release for google Play.
- HTML5 version with hosting somewhere.
- More different tile types: stone (eat ball), pointer (redirect), etc.

## Copyright

Carolingia (BigfooT) Font by Riky Vampdator:
- https://www.1001fonts.com/carolingia-bigfoot-font.html

Images and textures by various artists:
- https://opengameart.org/content/cannonball
- https://opengameart.org/content/basic-cannon-hd
- https://opengameart.org/content/target
- https://opengameart.org/content/default-iron
- https://opengameart.org/content/square-block-wall-seamless-texture-with-normalmap
- https://opengameart.org/content/2d-wooden-box
- https://opengameart.org/content/simple-seamless-tiles-of-dirt-and-sand
- https://opengameart.org/content/mostly-red-brick-wall-seamless-texture-with-normalmap
- https://opengameart.org/content/ui-buttons-0

Music and Sound Effects:
- https://opengameart.org/content/short-medieval-loop
- https://opengameart.org/content/big-explosion
- https://opengameart.org/content/win-sound-effect
- https://opengameart.org/content/collision-nutfall-yo-frankie

