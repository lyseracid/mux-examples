package com.mux.shooot.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.mux.shooot.Game

object DesktopLauncher {
    @JvmStatic
    fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration()
        config.title = "SHoooT!"
        config.width = Game.WIDTH.toInt()
        config.height = Game.HEIGHT.toInt()
        LwjglApplication(Game(), config)
    }
}