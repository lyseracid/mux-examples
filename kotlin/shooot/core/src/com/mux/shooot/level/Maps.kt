package com.mux.shooot.level

import kotlin.random.Random

typealias Map = Array<Array<TileData?>>

fun emptyMap(sizeX: Int, sizeY: Int) = Map(sizeX) { Array<TileData?>(sizeY) { null } }

class Maps {
    companion object {
        /** Generate a map by config. */
        fun random(config: MapConfig = MapConfig()): Map {
            val generator = MapGenerator(config)
            generator.run()
            return generator.map
        }

        /** Get predefined map (should be used for tutorials). */
        fun default(): Map {
            val map = emptyMap(3, 3)
            map[2][2] = TileData.Reflector(1).apply { isLocked = true }

            map[0][1] = TileData.Cannon()
            map[1][1] = TileData.Blank()
            map[2][1] = TileData.Blank().apply { isLocked = true }

            map[1][0] = TileData.Target()
            map[2][0] = TileData.Reflector(2).apply { isLocked }
            return map
        }
    }
}

class MinMax(val min: Int, val max: Int) {
    override fun toString() = "$min~$max"
}

data class MapConfig(
    var sizeX: Int = Random.nextInt(3, 5),
    var sizeY: Int = sizeX,
    var randomSeed: Int = Random.nextInt(0, Int.MAX_VALUE),
    var reflectors: MinMax = MinMax(2, (sizeX * sizeY) / 3),
    var empty: MinMax = MinMax(1, (sizeX * sizeY) / 2),
    var locks: MinMax = MinMax(1, (sizeX * sizeY) / 2),
    var shuffleMoves: Int = 100
) {
    val tiles get() = sizeX * sizeY
}

fun Random.nextInt(m: MinMax) = nextInt(m.min, m.max + 1)
fun Random.nextBoolean(chanceTrue: Float = 0.5f) = (nextFloat() <= chanceTrue)
fun <T> Random.nextChoice(vararg options: T) = options[nextInt(options.size)]
