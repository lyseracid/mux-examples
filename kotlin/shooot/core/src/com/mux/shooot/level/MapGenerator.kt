package com.mux.shooot.level

import com.badlogic.gdx.Gdx
import java.util.*
import kotlin.random.Random

class MapGenerator(private var c: MapConfig = MapConfig()) {
    private fun log(m: String) = Gdx.app.log("$this", m)

    val map = emptyMap(c.sizeX, c.sizeY)
    private val random = Random(c.randomSeed)

    private data class Id(var x: Int, var y: Int) {
        fun add(_x: Int, _y: Int) = Id(x + _x, y + _y)
        fun add(id: Id) = add(id.x, id.y)
    }

    private fun contains(id: Id) =
        id.x >= 0 && id.x < c.sizeX && id.y >= 0 && id.y < c.sizeY

    private fun place(id: Id, new: TileData) {
        val old = map[id.x][id.y]
        log("Place $new to $id" + if (old == null) "" else ", replacing $old")
        map[id.x][id.y] = new
    }

    private var current = Id(random.nextInt(0, c.sizeX), random.nextInt(0, c.sizeY))
    private var move = Id(random.nextChoice(1, -1), 0)

    private fun makeMove(s: Int) {
        if (s > directMoves) throw IndexOutOfBoundsException("$s > $directMoves")
        for (i in 1..s)
            do {
                current = current.add(move)
            } while (map[current.x][current.y] != null)
    }

    private val directMoves: Int
        get() {
            var count = 0
            var p = current.add(move)
            while (contains(p)) {
                val t = map[p.x][p.y]
                if (t == null) count += 1
                else if (t is TileData.Reflector) break
                p = p.add(move)
            }
            return count
        }

    private fun reflectorType(): Int {
        val topLeft = 0
        val topRight = 1
        val bottomRight = 2
        val bottomLeft = 3
        return when {
            move.x == 1 -> {
                move = Id(0, random.nextChoice(1, -1))
                if (directMoves == 0) move.y = -move.y
                if (move.y == 1) bottomRight else topRight
            }
            move.x == -1 -> {
                move = Id(0, random.nextChoice(1, -1))
                if (directMoves == 0) move.y = -move.y
                if (move.y == 1) bottomLeft else topLeft
            }
            move.y == 1 -> {
                move = Id(random.nextChoice(1, -1), 0)
                if (directMoves == 0) move.x = -move.x
                if (move.x == 1) topLeft else topRight
            }
            move.y == -1 -> {
                move = Id(random.nextChoice(1, -1), 0)
                if (directMoves == 0) move.x = -move.x
                if (move.x == 1) bottomLeft else bottomRight
            }
            else -> {
                throw ExceptionInInitializerError("Invalid move $move")
            }
        }
    }

    fun setPath() {
        // TODO: Add hot map (tiles where ball flies) to avoid accidental target placing on the way.
        // TODO: Move all this function only stuff right here.

        // Make sure we don't run into a wall at the start.
        if (directMoves == 0) move.x = -move.x
        place(current, TileData.Cannon(isMirrored = (move.x == -1)))

        val reflectorsLimit = random.nextInt(c.reflectors) + 1
        log("> Place up to $reflectorsLimit as many as possible")
        var reflectorCount = 0
        while (directMoves > 0 && reflectorCount < reflectorsLimit) {
            reflectorCount += 1
            makeMove(random.nextInt(1, directMoves + 1))
            place(current, TileData.Reflector(reflectorType()))
        }

        log("< Placed $reflectorCount reflectors, move $move, direct moves $directMoves")
        place(current, TileData.Target())
    }

    fun setBlanks() {
        // TODO: Merge common parts with setLocks().
        var empty = map.sumBy { line -> line.count { it == null } }
        val stayEmpty = random.nextInt(c.empty)
        log("> Reduce empty tiles from $empty to $stayEmpty")
        for (x in map.indices) {
            for (y in map[x].indices) {
                // The less tiles are left, the more chances to use.
                val pUsed = c.tiles.toFloat() / (c.tiles - (x * c.sizeY + y))
                // The more tiles should be used, the more chances to use.
                val pNeed = (empty - stayEmpty).toFloat() / empty
                if (map[x][y] == null && random.nextBoolean(pUsed * pNeed)) {
                    empty -= 1;
                    place(Id(x, y), TileData.Blank())
                }
            }
        }
        log("< Only $empty tiles left empty")
    }

    fun setLocks() {
        var unlocked = map.sumBy { line -> line.count { it != null } }
        val stayUnlocked = unlocked - random.nextInt(c.locks)
        log("> Reduce unlocked tiles from $unlocked to $stayUnlocked")
        for (x in map.indices) {
            for (y in map[x].indices) {
                val pUsed = c.tiles.toFloat() / (c.tiles - (x * c.sizeY + y))
                val pNeed = (unlocked - stayUnlocked).toFloat() / unlocked
                if (map[x][y] != null && random.nextBoolean(pUsed * pNeed)) {
                    unlocked -= 1;
                    map[x][y]!!.isLocked = true
                    log("Lock tile ${map[x][y]}")
                }
            }
        }
        log("< Only $unlocked tiles left unlocked")
    }

    fun shuffleUp() {
        val empty = MutableList<Id>(0) { throw ExceptionInInitializerError() }
        for (x in 0 until c.sizeX) for (y in 0 until c.sizeY)
            if (map[x][y] == null) empty.add(Id(x, y))

        log("Perform ${c.shuffleMoves} shuffle moves")
        var justMovedId = Id(-1, -1)
        var lastSpotId = Id(-1, -1)
        for (moveNumber in 0..c.shuffleMoves) {
            val e = random.nextInt(empty.size)
            val eId = empty[e]
            val candidates = listOf(
                eId.add(1, 0), eId.add(-1, 0), eId.add(0, 1), eId.add(0, -1)).filter {
                (eId != lastSpotId || it != justMovedId) && contains(it) && !(map[it.x][it.y]?.isLocked
                    ?: true)
            }
            if (candidates.isEmpty()) continue
            val tileId = candidates[random.nextInt(candidates.size)]

            // log("- Move $tileId to $eId")
            empty[e] = tileId
            map[eId.x][eId.y] = map[tileId.x][tileId.y]
            map[tileId.x][tileId.y] = null
            justMovedId = eId
            lastSpotId = tileId
        }
    }

    fun run() {
        log("Generate by $c")
        setPath()
        setBlanks()
        setLocks()
        // TODO: Test the map for auto-win and reshuffle a couple times.
        shuffleUp()
        // TODO: Test the map one again and may be regenerate on fail.
        map.withIndex().reversed().forEach { log("Generated ${Arrays.toString(it.value)}") }
    }
}