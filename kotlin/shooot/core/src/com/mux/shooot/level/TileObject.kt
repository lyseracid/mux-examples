package com.mux.shooot.level

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.mux.shooot.Resources
import com.mux.shooot.center
import com.mux.shooot.rect
import kotlin.math.abs
import kotlin.math.sign

// TODO: Make an actor?
/**
 * Represents a tile on the field.
 * @param data Specifies how the tile should look and behave.
 */
class TileObject(
    private val field: FieldActor,
    private val data: TileData,
    position: Vector2, size: Vector2
) : Rectangle(
    position.x, position.y, size.x, size.y
) {
    override fun toString(): String = "$data ${super.toString()}"
    private fun log(m: String) = Gdx.app.log("$this", m)

    /** Moves the tile in a specified direction. */
    fun move(vector: Vector2) {
        if (data.isLocked) return log("Tile is locked")

        val checkP = center.add(vector)
        if (!field.rect.contains(checkP)) return log("Position $checkP is not on the field")
        field.tile(checkP)?.also { return log("Target $checkP is blocked by $it") }

        Resources.fx.hit.play()
        setPosition(Vector2(x, y).add(vector))
        log("Moved")
    }

    /** Produces a cannon ball if this tile can do so. */
    fun fire(): BallObject? {
        if (data !is TileData.Cannon) return null
        val diameter = height / 4
        return BallObject(field,
            position = Vector2(
                x - diameter / 2 + if (data.isMirrored) 0f else width,
                y + height / 2 - diameter / 2),
            size = Vector2(diameter, diameter),
            speed = Vector2(2 * if (data.isMirrored) -width else width, 0f))
    }

    private var allowHitIn = 0f

    /** Should be called when the cannon ball hits this tile. */
    fun hit(ball: BallObject) {
        // Allow to hit the same tile only once a tile to avoid multi-hits.
        if (allowHitIn > 0) return
        allowHitIn = width / ball.speed.len()

        when (data) {
            is TileData.Target -> {
                Resources.fx.win.play()
                field.onWin()
            }
            is TileData.Reflector -> {
                val hitAngle = ball.speed.angle(data.normal)
                log("Hit angle $hitAngle, ball speed: ${ball.speed}, normal: ${data.normal}")
                if (abs(hitAngle) > 50)
                    return

                val rotation = -1 * sign(hitAngle) * (180 - 2 * abs(hitAngle))
                Resources.fx.hit.play()
                ball.center = center
                ball.speed = ball.speed.rotate(rotation)
                log("Reflected by $rotation, speed ${ball.speed}")
            }
            else -> {
            }
        }
    }

    fun draw(batch: Batch) {
        if (allowHitIn > 0)
            allowHitIn -= Gdx.graphics.deltaTime

        data.backTx?.also { tx ->
            batch.draw(tx, x, y, width, height)
        }
        data.frontTx?.also { tx ->
            batch.draw(
                tx, x, y, width / 2, height / 2, width, height,
                1f, 1f, data.frontRotation,
                0, 0, tx.width, tx.height, data.frontFlipX, data.frontFlipY)
        }
    }
}