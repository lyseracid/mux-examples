package com.mux.shooot.level

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.mux.shooot.Resources
import com.mux.shooot.cScl
import com.mux.shooot.center
import com.mux.shooot.position

/**
 * Cannon ball game object.
 */
class BallObject(
    private val field: FieldActor,
    position: Vector2,
    size: Vector2,
    var speed: Vector2
) : Rectangle(
    position.x, position.y, size.x, size.y
) {
    override fun toString(): String = "Ball ${super.toString()} -> $speed"
    private fun log(m: String) = Gdx.app.log("$this", m)

    /** Should be called each calculating frame. */
    fun move(dt: Float) {
        val move = speed.cScl(dt)
        position = position.add(move)

        field.tile(Vector2(x, y))?.also { tile ->
            if (center.sub(tile.center).len() < move.len())
                tile.hit(this)
        }
    }

    private val texure = TextureRegion(Resources.tx.ball)
    private var rotation = 0f
    fun draw(batch: Batch) {
        rotation -= speed.len() * Gdx.graphics.deltaTime
        batch.draw(
            texure, x, y, width / 2, height / 2, width, height,
            1f, 1f, rotation)
    }
}