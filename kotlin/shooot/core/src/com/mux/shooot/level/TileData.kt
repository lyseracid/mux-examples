package com.mux.shooot.level

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Vector2
import com.mux.shooot.Resources

/**
 * The tile description used for Map.
 */
sealed class TileData(var isLocked: Boolean = false) {
    override fun toString() = javaClass.simpleName + if (isLocked) "X" else ""

    abstract val frontTx: Texture?
    val backTx
        get() = if (isLocked) Resources.tx.bricks
        else if (frontTx != null) null else Resources.tx.iron

    open val frontRotation get() = 0f
    open val frontFlipX get() = false
    open val frontFlipY get() = false

    /** Empty tile, takes excessive space. */
    class Blank() : TileData() {
        override val frontTx get() = null
    }

    /** Shoots cannon balls. */
    class Cannon(val isMirrored: Boolean = false) : TileData() {
        override fun toString() = super.toString() + if (isMirrored) "M" else ""

        override val frontTx get() = Resources.tx.cannon
        override val frontFlipX get() = isMirrored
    }

    // TODO: Use enum from MapGenerator.reflectorType().
    /** Reflects cannon balls for a specific angle. */
    class Reflector(val angle: Int = 0) : TileData() {
        override fun toString() = super.toString() + "+$angle"

        override val frontTx get() = Resources.tx.reflector
        override val frontRotation get() = angle.toFloat() * -90f

        val normal = Vector2(-1f, 1f).rotate(frontRotation)
    }

    /** If cannon ball hits it, the game is won. */
    class Target() : TileData() {
        override val frontTx get() = Resources.tx.target
    }
}
