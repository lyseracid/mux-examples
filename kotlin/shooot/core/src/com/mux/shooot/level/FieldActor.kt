package com.mux.shooot.level

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.mux.shooot.*

/**
 * The game field. Controls the game flaw and all of the game objects with their interactions.
 * @param tileMap The map for initial game state.
 */
class FieldActor(
    var tileMap: Array<Array<TileData?>>
) : Actor() {
    private fun log(m: String) = Gdx.app.log("$this", m)

    var onWin: () -> Unit = {}

    private var tiles = MutableList<TileObject>(0) { throw ExceptionInInitializerError() }
    fun tile(p: Vector2) = tiles.find { it.contains(p) }
    fun swipe(p: Vector2, d: Vector2) = tile(p.add(x, y))?.move(d.scl(tileSize))

    private var ball: BallObject? = null

    /** User wants the cannon to fire. */
    fun fire() {
        Resources.fx.fire.play()
        tiles.forEach { it.fire()?.apply { ball = this } }
        log("Fire $ball")
    }

    override fun act(dt: Float) {
        ball?.move(dt)
    }

    override fun draw(batch: Batch?, parentAlpha: Float) {
        batch!!.apply {
            // This is here instead of act because there coordinate system is not ready yet.
            if (tiles.isEmpty()) resetTiles()

            draw(Resources.tx.dirt, x, y, width, height)
            tiles.forEach { it.draw(this) }
            ball?.draw(this)
        }
    }

    private val tileSize get() = size.cDiv(Vector2(tileMap.size.toFloat(), tileMap[0].size.toFloat()))
    fun resetTiles() {
        tiles.clear()
        ball = null
        tileMap.forEachIndexed { xn, line ->
            line.forEachIndexed { yn, tile ->
                tile?.also {
                    tiles.add(TileObject(
                        this, it, position.cAdd(xn * tileSize.x, yn * tileSize.y), tileSize))
                }
            }
        }
        log("Spawned ${tiles.size} tiles: $tiles")
    }
}