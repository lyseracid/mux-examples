package com.mux.shooot

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont

class Resources {
    companion object {
        lateinit var tx: Textures
        lateinit var fx: Effects
        lateinit var tracks: Tracks
        lateinit var font: BitmapFont

        fun initialize() {
            tx = Textures()
            fx = Effects()
            tracks = Tracks()
            font = BitmapFont().apply {
                color = Color(0.525f, 0.09f, 0.09f, 1f)
            }
        }

        var playing: Music? = null
            set(value) {
                field?.stop()
                field = value
                field?.isLooping = true
                field?.play()
            }

        // TODO: Dispose?
    }

    class Textures {
        val ball = load("ball")
        val box = load("box")
        val bricks = load("bricks")
        val cannon = load("cannon")
        val dirt = load("dirt")
        val exit = load("exit")
        val iron = load("iron")
        val logo = load("logo")
        val reflector = load("reflector")
        val reset = load("reset")
        val sand = load("sand")
        val target = load("target")

        private fun load(n: String) = Texture("images/$n.png")
    }

    class Effects {
        val fire: Sound = load("fire")
        val hit: Sound = load("hit")
        val win: Sound = load("win")

        private fun load(n: String) = Gdx.audio.newSound(Gdx.files.internal("sound/$n.mp3"))
    }

    class Tracks {
        val folk: Music = load("folk")

        private fun load(n: String) = Gdx.audio.newMusic(Gdx.files.internal("music/$n.mp3"))
    }
}
