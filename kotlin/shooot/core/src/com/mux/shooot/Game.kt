package com.mux.shooot

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.Stage
import com.mux.shooot.ui.MenuStage

class Game : ApplicationAdapter() {
    companion object {
        val WIDTH = 600f
        val HEIGHT = 800f
    }

    private var _stage: Stage? = null
    var stage: Stage
        get() = _stage!!
        set(value) {
            log("Set stage $value")
            Gdx.input.inputProcessor = value
            _stage?.dispose();
            _stage = value
        }

    fun log(m: String) = Gdx.app.log("Game", m)

    override fun create() {
        Resources.apply {
            initialize()
            playing = tracks.folk
        }
        stage = MenuStage(this)
    }

    override fun render() {
        stage.act(Gdx.graphics.deltaTime);

        Gdx.gl.glClearColor(0.827f, 0.682f, 0.424f, 0f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        stage.viewport.apply()
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        log("Resize to ${width}X${height}")
        stage.viewport.update(width, height, true)
    }

    override fun dispose() {
        log("Dispose")
        stage.dispose()
        // TODO: Resources.dispose()
    }
}