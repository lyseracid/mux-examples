package com.mux.shooot.ui

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.viewport.FitViewport
import com.mux.shooot.*

/**
 * The main menu stage.
 */
class MenuStage(
    private var game: Game
) : Stage(
    FitViewport(Game.WIDTH, Game.HEIGHT)
) {
    private fun log(m: String) = Gdx.app.log("$this", m)

    private val playButton = makeButton(Resources.tx.logo)

    init {
        log("Init")
        addActor(Table().apply {
            background = TextureRegionDrawable(Resources.tx.sand)
            setFillParent(true)
            add(playButton).width(viewport.size.x / 2).height(viewport.size.x / 2)
        })
        playButton.onClick {
            log("Start Game")
            game.stage = LevelStage(game)
        }
    }
}