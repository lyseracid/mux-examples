package com.mux.shooot.ui

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.viewport.FitViewport
import com.mux.shooot.*
import com.mux.shooot.level.FieldActor
import com.mux.shooot.level.Maps
import kotlin.math.abs
import kotlin.math.sign

/**
 * The stage where all of the gameplay happens.
 */
class LevelStage(
    private var game: Game
    // TODO: Add MapConfig or already generated Map?
) : Stage(
    FitViewport(Game.WIDTH, Game.HEIGHT)
) {
    private fun log(m: String) = Gdx.app.log("$this", m)

    // TODO: Map type should be selectable, use Maps.default() for debug.
    private val gameField = FieldActor(Maps.random())
    private val resetButton = makeButton(Resources.tx.box, Resources.tx.reset)
    private val fireButton = makeButton(Resources.tx.logo)
    private val exitButton = makeButton(Resources.tx.box, Resources.tx.exit)

    init {
        log("Init")
        addActor(Table().apply {
            background = TextureRegionDrawable(Resources.tx.sand)
            setFillParent(true)
            add(gameField).size(squareVector(viewport.size.x * 0.95f))
            row()
            add(Table().apply {
                add(resetButton).size(squareVector(viewport.size.x / 5))
                    .pad(viewport.size.x * 0.05f)
                add(fireButton).size(squareVector(viewport.size.x / 3))
                    .pad(viewport.size.x * 0.01f)
                add(exitButton).size(squareVector(viewport.size.x / 5))
                    .pad(viewport.size.x * 0.05f)
            })
        })
        gameField.onSwipe { touch, distance ->
            gameField.swipe(
                touch,
                if (abs(distance.x) > abs(distance.y)) Vector2(sign(distance.x), 0f)
                else Vector2(0f, sign(distance.y)))
        }
        gameField.onWin = {
            log("Victory")
            gameField.tileMap = Maps.random()
            gameField.resetTiles()
        }
        resetButton.onClick { gameField.resetTiles() }
        fireButton.onClick { gameField.fire() }
        exitButton.onClick { game.stage = MenuStage(game) }
    }

    private fun stackButton(size: Vector2, vararg textures: Texture) {

    }
}