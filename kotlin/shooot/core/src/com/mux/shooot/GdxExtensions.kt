package com.mux.shooot

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.badlogic.gdx.scenes.scene2d.ui.Cell
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Stack
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.badlogic.gdx.utils.viewport.Viewport
import kotlin.math.abs
import kotlin.math.min

// TODO: Comments?

// Just lake build-in Vector2 functions but without this modifications.

fun Vector2.cAdd(v: Vector2) = cpy().add(v)
fun Vector2.cAdd(x: Float, y: Float) = cAdd(Vector2(x, y))

fun Vector2.cSub(v: Vector2) = cpy().sub(v)
fun Vector2.cSub(x: Float, y: Float) = cSub(Vector2(x, y))

fun Vector2.cScl(s: Float) = cpy().scl(s)
fun Vector2.cScl(v: Vector2) = cpy().scl(v)
fun Vector2.cScl(x: Float, y: Float) = cScl(Vector2(x, y))

fun Vector2.cDiv(s: Float) = Vector2(x / s, x / s)
fun Vector2.cDiv(v: Vector2) = Vector2(x / v.x, x / v.y)
fun Vector2.cDiv(x: Float, y: Float) = cScl(Vector2(x, y))

fun Vector2.closeAngle(v: Vector2) = min(abs(angle(v)), abs(v.angle(this)))
fun squareVector(v: Float) = Vector2(v, v)

// Kotlin like getters and setters for Rectangle.

var Rectangle.position: Vector2
    get() = getPosition(Vector2())
    set(v) {
        setPosition(v)
    }

var Rectangle.size: Vector2
    get() = getSize(Vector2())
    set(v) {
        setSize(v.x, v.y)
    }

var Rectangle.center: Vector2
    get() = getCenter(Vector2())
    set(v) {
        setCenter(v)
    }

// Rectangle like getters and setters for Actor.

val Actor.rect: Rectangle
    get() = Rectangle(x, y, width, height)

var Actor.position: Vector2
    get() = Vector2(x, y)
    set(v) {
        x = v.x; y = v.y
    }

var Actor.size: Vector2
    get() = Vector2(width, height)
    set(v) {
        width = v.x; height = v.y
    }

var Actor.center: Vector2
    get() = position.cAdd(size.cDiv(2f))
    set(v) {
        position = v.cSub(size.cDiv(2f))
    }

// Actor actions.

fun makeButton(vararg textures: Texture): Button {
    val stack = Stack()
    textures.forEach { stack.add(Image(it)) }
    return Button(stack, Button.ButtonStyle())
}

fun Actor.onClick(handle: () -> Unit) = addListener(object : ChangeListener() {
    override fun changed(event: ChangeEvent?, actor: Actor?) {
        handle()
    }
})

fun Actor.onSwipe(
    handle: (touch: Vector2, distance: Vector2) -> Unit
) = addListener(object : ActorGestureListener() {
    private var touch = Vector2()

    override fun touchDown(e: InputEvent?, x: Float, y: Float, p: Int, b: Int) {
        touch = Vector2(x, y)
    }

    override fun touchUp(e: InputEvent?, x: Float, y: Float, p: Int, b: Int) {
        val distance = Vector2(x, y).sub(touch)
        if (distance.len() > (width + height) / 20)
            handle(touch, distance)
    }
})

// Etc.

val Viewport.size: Vector2
    get() = Vector2(worldWidth, worldHeight)

fun <T : Actor> Cell<T>.size(v: Vector2) = width(v.x).height(v.y)