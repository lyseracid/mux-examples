#!/bin/env python3
''' Patches Thinkpad Compact USB Keyboard firmware so left CTRL and FN keys are swapped.
    Usage: <script> [source_path] [destination_path]
    Get firmware: https://support.lenovo.com/ru/en/accessories/pd026745
    More info: https://github.com/lentinj/tp-compact-keyboard/issues/32#issuecomment-687659110
'''

import hashlib
import sys


DEFAULT_NAME = 'tp_compact_usb_kb_with_trackpoint_fw.exe'
SOURCE_SHA = '7116a3819ee094857d21e4671cb6cf953d582372126f0f6728f6b2421eda7bd4'
RESULT_SHA = '123143092dab578550c87a62526b07a6c5f06c047f2455be87971aa51577e300'

LEFT_CTRL_ADDR = 0x74004 
LEFT_CTRL_CODE = 0xba

FN_ADDR = 0x740BA 
FN_CODE = 0xf5


def expect_eq(name, expected, actual):
    if expected != actual:
        raise AssertionError('Wrong %s: expected %r, actual %r' % (name, expected, actual))


def change_byte(data, addr, value):
    return data[:addr] + value.to_bytes(1, 'big') + data[addr + 1:]


def patch(data):
    expect_eq('source SHA256', SOURCE_SHA, hashlib.sha256(data).hexdigest()) 
    expect_eq('current left CTRL code', data[LEFT_CTRL_ADDR], LEFT_CTRL_CODE)
    expect_eq('current FN code', data[FN_ADDR], FN_CODE)
    
    data = change_byte(data, LEFT_CTRL_ADDR, FN_CODE)
    data = change_byte(data, FN_ADDR, LEFT_CTRL_CODE)
    expect_eq('result SHA256', RESULT_SHA, hashlib.sha256(data).hexdigest())
    return data

    
if __name__ == '__main__':
    source = sys.argv[1] if len(sys.argv) > 1 else DEFAULT_NAME
    print('Source file: ' + source)
    with open(source, 'rb') as f: data = f.read()
    
    patched = sys.argv[2] if len(sys.argv) > 2 else (source[:-4] + '_patched.exe')
    with open(patched, 'wb') as f: f.write(patch(data))
    print('Patched firmware is ready to install: ' + patched)
