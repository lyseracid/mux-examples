use std::collections::{LinkedList, HashMap, hash_map::Entry};
use std::net::SocketAddr;

use log::*;
use tokio::net::{TcpListener, TcpStream};
use tokio::prelude::*;

pub struct Connector {
    listener: TcpListener,
    storage: HashMap<String, LinkedList<TcpStream>>,
}

impl Connector {
    pub async fn new(bind_addr: &SocketAddr) -> io::Result<Connector> {
        let listener = TcpListener::bind(bind_addr).await?;
        return Ok(Connector{listener, storage: HashMap::new()})
    }

    pub fn addr(&self) -> SocketAddr {
        self.listener.local_addr().unwrap()
    }

    pub async fn connect(&mut self, service: &str) -> io::Result<TcpStream> {
        {
            let streams = self.streams(service.to_owned());
            if let Some(stream) = streams.pop_front() {
                trace!("Return saved RC to {}: {:?} ({} left)", service, stream, streams.len());
                return Ok(stream);
            }
        }
        loop {
            match self.accept_stream().await {
                Ok((actual_service, stream)) => {
                    if actual_service == service {
                        trace!("Return new RC to {}: {:?}", service, stream);
                        return Ok(stream)
                    }
                    trace!("{} != {}", actual_service, service);
                    let streams = self.streams(actual_service.clone());
                    trace!("Save RC to {}: {:?} ({} total)",
                        actual_service, stream, streams.len() + 1);
                    streams.push_back(stream);
                },
                Err(e) => debug!("Unable to accept RC: {}", e),
            }
        }
    }

    fn streams(&mut self, service: String) -> &mut LinkedList<TcpStream> {
        match self.storage.entry(service) {
            Entry::Occupied(o) => o.into_mut(),
            Entry::Vacant(v) => v.insert(LinkedList::new()),
        }
    }

    async fn accept_stream(&mut self) -> io::Result<(String, TcpStream)> {
        trace!("Wait for more RCs on {:?}", self.listener);
        let (mut stream, _) = self.listener.accept().await?;
        let mut buf = [0; 255];
        let mut n = 0;
        while n == 0 || buf[n - 1] != 0 {
            n += stream.read(&mut buf).await?;
        }
        match std::str::from_utf8(&buf[0..n - 1]) {
            Ok(s) => Ok((s.to_owned(), stream)),
            Err(e) => Err(io::Error::new(io::ErrorKind::InvalidData, e)),
        }
    }
}

pub struct Server {
    pub service: String,
    pub receiver_addr: SocketAddr,
}

impl Server {
    pub fn new(service: String, receiver_addr: SocketAddr) -> Server {
        // TODO: Start async connects, so we have some even before accept.
        Server{service, receiver_addr}
    }

    pub async fn accept(&mut self) -> io::Result<TcpStream> {
        loop {
            // TODO: Initiate new streams in parallel so we always have some streams available.
            match self.next_stream().await {
                Ok(s) => return Ok(s),
                // TODO: Introduce delay before reconnect attempt.
                // TODO: Return failure if several attempts in a row have failed.
                Err(e) => debug!("RC {} to {} failed: {}", self.service, self.receiver_addr, e),
            }
        }
    }

    async fn next_stream(&self) -> io::Result<TcpStream> {
        trace!("Connect RC {} to {}", self.service, self.receiver_addr);
        let mut stream = TcpStream::connect(self.receiver_addr).await?;
        stream.write_all(self.service.as_bytes()).await?;
        stream.write_all(&[0]).await?;
        // TODO: Implement keep alive?
        Ok(stream)
    }
}

#[tokio::test]
async fn main() {
    let _ = env_logger::try_init();
    let any_ipv4_port = SocketAddr::from((std::net::Ipv4Addr::LOCALHOST, 0));
    let service_name = "my-cool-service".to_owned();
    let test_data = b"Test Data";

    let mut connector = Connector::new(&any_ipv4_port).await.unwrap();
    let mut server = Server::new(service_name.clone(), connector.addr());
    let mut server_stream = server.accept().await.unwrap();
    let mut client_stream = connector.connect(&service_name).await.unwrap();

    let mut buf = [0; 255];
    client_stream.write_all(test_data).await.unwrap();
    assert_eq!(server_stream.read(&mut buf).await.unwrap(), test_data.len());
    assert_eq!(&buf[0..test_data.len()], test_data);
}
