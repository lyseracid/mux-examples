//use tokio::prelude::*;
//use std::net::{SocketAddr, IpAddr, Ipv4Addr};

mod gateway;
mod reverse_connect;

#[tokio::main]
async fn main() {
    match gateway::Server::new("0.0.0.0:7777").await {
        Ok(mut server) => {
            println!("* Start server at {}", server.addr());
            server.run().await.unwrap();
        },
        Err(error) => {
            println!("* Unable to start server: {}", error);
            return;
        }
    };
}
