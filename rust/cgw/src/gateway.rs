use tokio::prelude::*;

#[derive(Debug)]
pub struct Server {
    listener: tokio::net::TcpListener,
}

impl Server {
    pub async fn new(addr: &str) -> io::Result<Server> {
        let listener = tokio::net::TcpListener::bind(addr).await?;
        Ok(Server{listener})
    }

    pub fn addr(&self) -> std::net::SocketAddr {
        self.listener.local_addr().unwrap()
    }

    pub async fn run(&mut self) -> io::Result<()> {
        eprintln!("Start {:?}", self.listener);
        loop {
            let mut stream = match self.listener.accept().await {
                Ok((stream, _)) => stream,
                Err(e) => {
                    eprintln!("Stop {:?} -> {:?}", self.listener, e);
                    return Err(e);
                }
            };
            tokio::spawn(async move {
                eprintln!("Connected {:?}", stream);
                let reason = serve_stream(&mut stream).await;
                eprintln!("Disconnected {:?} -> {:?}", stream, reason);
            });
        }
    }
}

async fn serve_stream(stream: &mut tokio::net::TcpStream) {
    let mut buf = [0; 1024];
    loop {
        let n = match stream.read(&mut buf).await {
            Ok(n) if n == 0 => return,
            Ok(n) => n,
            Err(e) => {
                eprintln!("failed to read from socket; err = {:?}", e);
                return;
            }
        };
        if let Err(e) = stream.write_all(&buf[0..n]).await {
            eprintln!("failed to write to socket; err = {:?}", e);
            return;
        }
    }
}

#[tokio::test]
async fn main() {
    let mut server = Server::new("127.0.0.1:0").await.unwrap();
    let addr = server.addr();
    tokio::spawn(async move { server.run().await.unwrap(); });

    let mut client = tokio::net::TcpStream::connect(addr).await.unwrap();
    let test_data = b"Test Data";
    client.write_all(test_data).await.unwrap();

    let mut buf = [0; 255];
    assert_eq!(client.read(&mut buf).await.unwrap(), test_data.len());
    assert_eq!(&buf[0..test_data.len()], test_data);
}
