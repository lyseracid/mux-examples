# MUX Cloud GateWay

The HTTP proxy service to grant network access regardless of NATs.

## Workflow

```plantuml
actor "3rd-party Client" as client #white
participant "Cloud Proxy\nin Internet" as cloud #lightgreen
collections "Server GateWays\nbehind NAT" as gateway #lightgreen
collections "3rd-party Servers\nbehind NAT" as server
actor "Administrator" as admin #white

admin -> gateway : Configure: service_name -> local IP
gateway -> cloud : Upgrade to RC (service_name)\nTCP connection will be saved

client -> cloud : HTTP Proxy-Request to service_name\nby public DNS
cloud -> gateway : HTTP Proxy-Request to service_name\nby saved TCP connection
gateway -> server : HTTP Request by local IP
server -> gateway : HTTP Response
gateway -> cloud : HTTP Response
cloud -> client : HTTP Response
gateway -> cloud : Upgrade to RC (service_name)\nto have enough connections
```
